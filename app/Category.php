<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    public function post(){
        return $this->belongsToMany('App\Post', 'category_posts', 'category_id', 'post_id');
    }
}
