<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use Auth;

class CommentController extends Controller
{
    public function store(Request $request, $id)
    {
        //$post = Post::where('id',$id)->first();
        $request->validate([
            'comment' => 'required|max:255'
        ]);

        $posts = Post::find($request['post_id']);
        $comment = new Comment;
        $user = Auth::user();

        $comment->body = $request['comment'];
        $user->comment()->save($comment);
        $posts->comment()->save($comment);

        return redirect()->back();
    }
}
