<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Category;
use App\Post;

class CategoryController extends Controller
{
    public function index($id){
        $category = Category::find($id);
        $limit_str = new Str;
        
        return view('pages.category_index', compact('category','limit_str'));
    }
}
