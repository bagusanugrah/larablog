<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Post;
use App\Category;
use App\Comment;
use App\Category_post;
use App\User;
use Auth;
use DB;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        $limit_str = new Str;

        return view('pages.index', compact('posts','limit_str'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'body' => 'required|max:15000',
            'categories' => 'required|max:255'
        ]);

        $post = Post::create([
            'title' => $request['title'],
            'body' => $request['body']
        ]);
        
        //explode untuk mengubah string menjadi array dengan pemisah koma
        if($request['categories'] != ""){
            $categories = explode(',', $request['categories']);
        
            $category_ids = [];
            foreach($categories as $perkategori){
                $category = Category::where('name', $perkategori)->first();
                if($category){
                    $category_ids[] = $category->id;
                } else {
                    $new_category = Category::create(['name' => $perkategori]);
                    $category_ids[] = $new_category->id;
                }
            }

            $post->category()->sync($category_ids);
        }

        $user = Auth::user();
        $user->post()->save($post);

        return redirect('/home')->with('success','Postingan Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $posts = Post::find($id);
        
        return view('pages.show', compact('posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = Post::find($id);
        $arr_baru = [];
        foreach($posts->category as $perkategori){
            $arr_baru[] = $perkategori->name;
        }
        
        $categories_str = implode(",", $arr_baru);

        $posts->category = $categories_str;
        return view('pages.edit', compact('posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'body' => 'required|max:15000',
            'categories' => 'required|max:255'
        ]);
        
        $update = Post::where("id", $id)->update([
            "title"=>$request["title"],
            "body"=>$request["body"]
        ]);
        $post = Post::where("id", $id)->first();

        if($request['categories'] != ""){
            $categories_arr = explode(',', $request['categories']);
            
            $category_ids = [];
            foreach($categories_arr as $perkategori){
                $category = Category::where('name', $perkategori)->first();
                if($category){
                    $category_ids[] = $category->id;
                } else {
                    $new_category = Category::create(['name' => $perkategori]);
                    $category_ids[] = $new_category->id;
                }
            }

            $post->category()->sync($category_ids);
        }
        else{
            DB::table('category_posts')->where('post_id',$id)->delete();
        }

        $user = Auth::user();
        $user->post()->save($post);
        
        return redirect('/home')->with('success','Berhasil Update post!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('category_posts')->where('post_id',$id)->delete();
        Post::destroy($id);
        
        return redirect('/home')->with('success','Post Berhasil Dihapus!');
    }
}
