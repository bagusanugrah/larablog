<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function comment(){
        return $this->hasMany('App\Comment', 'post_id');
    }

    public function category(){
        return $this->belongsToMany('App\Category', 'category_posts','post_id','category_id');
    }
}
