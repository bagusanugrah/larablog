@extends('template.master')

@section('title')
<title>Postingan Saya | LaraBlog</title>
@endsection

@section('content')
<div class="mt-3 ml-3 mr-3">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Postingan Saya</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                  <div class="alert alert-success">
                    {{ session('success') }}
                  </div>
                @endif
                <a class="btn btn-primary" href="{{route('posts.create')}}">Buat Postingan Baru</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Judul</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($posts->reverse() as $key => $post)
                    <tr>
                      <td><a href="{{route('posts.show', ['post' => $post->id])}}">{{ $post->title }}</a></td>
                      @if($post->user_id == Auth::id())
                      <td style="display: flex;">
                        <a href="{{route('posts.edit', ['post' => $post->id])}}" class="btn btn-default btn-sm ml-1 mr-1">edit</a>
                        <form action="{{route('posts.destroy', ['post' => $post->id])}}" method="POST">
                          @csrf
                          @method('DELETE')
                          <input type="submit" value="delete" class="btn btn-danger btn-sm ml-1 mr-1">
                        </form>
                      </td>
                      @endif
                      @if($post->user_id != Auth::id())
                      <td></td>
                      @endif
                    </tr>
                    @empty
                    <tr>
                      <td colspan="4" align="center">Tidak ada pertanyaan</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
</div>  
</div>
@endsection