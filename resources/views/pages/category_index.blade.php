@extends('template.master')

@section('title')
<title>Kategori {{ $category->name }} | LaraBlog</title>
@endsection

@section('content')
    @forelse($category->post->reverse() as $post)
      <div class="mt-3 ml-3 mr-3">
          <div class="card">
              <div class="card-header">
                <h3><a href="{{route('posts.show', ['post' => $post->id])}}" style="color: black;">{{$post->title}}</a></h3>
              </div>
              <div class="card-body">
                <div class="text-muted">
                    <p>
                    <small>Diposting pada: {{$post->created_at}}&nbsp;&nbsp;</small>
                    <small>Diubah pada: {{$post->updated_at}}&nbsp;&nbsp;</small>
                    <small>Oleh: {{$post->user->name}}  </small>
                    </p>
                </div>
                <p>
                  {!! $limit_str->limit($post->body, $limit = 1000, $end = '...') !!}
                  @if (strlen($post->body) > 1000)
                  <a href="{{route('posts.show', ['post' => $post->id])}}">Baca Selengkapnya</a>
                  @endif
                </p>
                <p>
                  Kategori:&nbsp;
                  @forelse($post->category as $percategory)
                    <a href="/category/{{{$percategory->id}}}" class="btn btn-sm btn-success">{{$percategory->name}}</a>
                    @empty
                    Tidak ada kategori
                  @endforelse
                </p>
              </div>
          </div>
      </div>
      @empty
      <div></div>
      <div class="mt-3 ml-3 mr-3">
        <div class="card">
          <div class="mb-3"></div>
          <p align="center"><b>Tidak ada post</b></p>
        </div>
      </div>
    @endforelse
@endsection