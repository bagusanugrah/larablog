@extends('template.master')

@section('title')
<title>{{$posts->title}} | LaraBlog</title>
@endsection

@section('content')
<div class="mt-3 ml-3 mr-3">
    <div class="card card-widget">
        <div class="card-header">
        <div class="user-block">
            <p class="h3">{{$posts->title}}</p>
        </div>
        @guest
            @if (Route::has('register'))
            @endif
            @else
                @if(Auth::user()->id == $posts->user->id)
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" title="Edit">
                    <a href="{{route('posts.edit', ['post' => $posts->id])}}"><i class="fas fa-edit"></i></a>
                    </button>
                </div>
                @endif
        @endguest
        </div>
        
        <div class="card-body">
            <span class="float-right text-muted" style="display: block;">
                <div class="callout callout-info">
                <div class="row">
                    <p><small>Diposting pada: {{$posts->created_at}}</small></p>
                </div>
                <div class="row">
                    <p><small>Diubah pada: {{$posts->updated_at}}</small></p>
                </div>
                <div class="row">
                    <p><small>Oleh: {{$posts->user->name}}</small></p>
                </div>
                </div>
            </span>
            <p>
            {!! $posts->body !!}
            </p>
            <p>
              Kategori:&nbsp;
              @forelse($posts->category as $percategory)
                <a href="/category/{{{$percategory->id}}}" class="btn btn-sm btn-success">{{$percategory->name}}</a>
                @empty
                Tidak ada kategori
              @endforelse
            </p>
        </div>
    <!--<h4>{{$posts->title}}</h4>
    <p>Penanya: {{$posts->user->name}}</p>
    <p>{{$posts->body}}</p>
    <p>Tags:
        @forelse($posts->category as $perkategori)
            <button class="btn btn-primary btn-sm"> {{$perkategori->name}} </button>
            @empty
            No tags
        @endforelse
    </p>-->
    </div>
</div>
@endsection

@section('comment')
<div class="card-footer card-comments">
      <div class="post">
        <strong class="mb-3">Comment ({{$posts->comment->count()}})</strong>
      </div>
      @foreach($posts->comment->reverse() as $percomment)
        <div class="card-comment">
          <span class="username">
            {{ $percomment->user->name }}
          <span class="text-muted float-right">{{ $percomment->created_at }}</span>
          </span>
            {{ $percomment->body }}
        </div>
      @endforeach
    </div>
@guest
    @if (Route::has('register'))
    <p align="center">Login terlebih dahulu agar bisa berkomentar</p>
    @endif
    @else
    <div class="card-footer">
      <form action="/posts/{post}" method="post">
        @csrf
        <input type="hidden" name="post_id" value="{{$posts->id}}">

        <div class="img-push row">
          <div class="input-group">
            <input type="text" class="disabled form-control" name="comment" placeholder="Tambahkan komentar">
            <button type="submit" class="btn btn-info ml-2" style="border-radius:50%">
              <i class="fa fa-location-arrow"></i>
            </button>
          </div>
        </div>
      </form>
    </div>
@endguest
@endsection