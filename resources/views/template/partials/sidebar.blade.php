<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{asset('/adminlte/dist/img/laravel.png')}}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">LaraBlog</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('/adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <span class="d-block" style="color: white;">
            @guest
              @if (Route::has('register'))
              Tamu
              @endif
              @else
              {{Auth::user()->name}}
            @endguest
          </span>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          @guest
            @if (Route::has('register'))
            @endif
            @else
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Halaman Admin
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/home" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Postingan Saya</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/posts/create" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Buat Postingan Baru</p>
                </a>
              </li>
            </ul>
          </li>
          @endguest
          
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Kategori
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            @forelse(\App\Category::all()->reverse() as $percategory)
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/category/{{{$percategory->id}}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ $percategory->name }}</p>
                </a>
              </li>
            </ul>
            @empty
            <ul class="nav nav-treeview">
              <p class="ml-4" style="color: white;">Belum ada kategori</p>
            </ul>
            @endforelse
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>